package com.example.alunos.androidtoolbar.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.alunos.androidtoolbar.R;

class PessoaViewHolder extends RecyclerView.ViewHolder {
    final TextView nome;
    final TextView email;

    public PessoaViewHolder(@NonNull View itemView) {
        super(itemView);
        nome = itemView.findViewById(R.id.nome);
        email = itemView.findViewById(R.id.email);
    }
}


