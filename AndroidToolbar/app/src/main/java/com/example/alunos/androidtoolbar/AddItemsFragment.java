package com.example.alunos.androidtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alunos.androidtoolbar.adapter.PessoaAdapter;
import com.example.alunos.androidtoolbar.model.Pessoa;

import java.util.ArrayList;

public class AddItemsFragment extends Fragment {
    private ArrayList<Pessoa> listaPessoas;
    RecyclerView rv;
    MainActivity ativ;
    EditText nome;
    EditText email;
    Button btn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ativ = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_add_items_layout, container, false);
        nome = view.findViewById(R.id.enome);
        email = view.findViewById(R.id.eemail);
        rv = view.findViewById(R.id.rv);
        btn = view.findViewById(R.id.bsalvar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String n = nome.getText().toString();
                    String e = email.getText().toString();

                    listaPessoas = ativ.getLista();

                    if(!n.matches("") && !e.matches("")) {
                        listaPessoas.add(new Pessoa(n, e));
                        PessoaAdapter p = ativ.getAdapter();
                        p.notifyDataSetChanged();
                    }else{
                        Toast toast = Toast.makeText(ativ.getApplicationContext(), "Faltando informações...", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    
                } catch (Exception e) {
                    Log.d("Deu erro!", e.getMessage());
                    return;
                }
            }
        });
        return view;
    }
}
