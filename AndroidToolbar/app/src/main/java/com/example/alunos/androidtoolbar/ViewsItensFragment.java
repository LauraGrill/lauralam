package com.example.alunos.androidtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alunos.androidtoolbar.adapter.PessoaAdapter;
import com.example.alunos.androidtoolbar.model.Pessoa;

import java.util.ArrayList;

public class ViewsItensFragment extends Fragment {

    private ArrayList<Pessoa> listaPessoas;
    RecyclerView rv;
    PessoaAdapter p;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MainActivity atividade = (MainActivity) getActivity();
        View v = inflater.inflate(R.layout.fragment_view_items_layout, container, false);
        rv = v.findViewById(R.id.recyclerview);
        listaPessoas = atividade.getLista();

        p = atividade.getAdapter();
        rv.setAdapter(p);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(atividade.getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        rv.setLayoutManager(layout);


        return v;
    }
}
