package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alunos.meuslivros.R;

public class LivroViewHolder extends RecyclerView.ViewHolder {
    final TextView titulo;
    final TextView autor;
    final TextView descricao;

    public LivroViewHolder(View itemView) {
        super(itemView);
        titulo = itemView.findViewById(R.id.txtTitulo);
        autor = itemView.findViewById(R.id.txtAutor);
        descricao = itemView.findViewById(R.id.txtDescricao);
    }
}

@NonNull
@Override
public RecyclerView.ViewHolder onCreateViewHolder(
        @NonNull ViewGroup parent, int viewType
        ) {
    View itemView = LayoutInflater.from(context)
            .inflate(R.layout.item_lista, parent,false);
    return new LivroViewHolder(itemView);
}

@Override
public int getItemCount (){
    return lista.size();-
}

@Override
public void onBindViewHolder(
        @NonNull RecyclerView.ViewHolder holder, int position
){
    LivroViewHolder theHolder = (LivroViewHolder) holder;
    Livro livro = lista.get( position );
    theHolder.titulo.setText(livro.getTitulo() );
    theHolder.autor.setText(livro.getAutor());
    theHolder.descricao.setText(livro.getDescricao());
}