import java.util.Scanner;

public class NumeroDecrescente{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int x = Integer.parseInt(input.next());
        while(x >= 0){
            System.out.println(x);
            x--;
        }
    }   
}
    
