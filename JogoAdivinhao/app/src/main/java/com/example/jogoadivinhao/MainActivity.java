package com.example.jogoadivinhao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private int sort;
    private Random rand = new Random();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public int Inserir() {
        EditText palpite = findViewById(R.id.valor);
        String v = palpite.getText().toString();
        int valor = Integer.parseInt(v);
        return valor;
    }

    public void Sortear(View r) {
        sort = rand.nextInt(51);
    }

    public void Palpite(View p) {
        int value = Inserir();
        if (value == sort) {
            TextView res = findViewById(R.id.res);
            res.setText("Parabéns!! Você acertou!!");
        } else if (value <sort) {
            TextView res = findViewById(R.id.res);
            res.setText("Xiii... Palpite menor do que o sortedo Tente novamente!");
        } else {
            TextView res = findViewById(R.id.res);
            res.setText("Xiii... Palpite maior do que o sortedo Tente novamente!");
        }
    }

}