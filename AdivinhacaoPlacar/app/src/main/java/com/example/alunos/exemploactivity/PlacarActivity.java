package com.example.alunos.exemploactivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PlacarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placar);

        Intent intencao = getIntent();
        Bundle pacote = intencao.getExtras();

        if(pacote != null){
            int tentativa = pacote.getInt("cont");
            SharedPreferences Placar = getPreferences(Context.MODE_PRIVATE);
            Context context = getApplicationContext();
            int record = Placar.getInt("record",1000);
            int placar1  = Placar.getInt("placar1", 0);
            int placar2  = Placar.getInt("placar2", 0);
            int placar3  = Placar.getInt("placar3", 0);
            int placar4  = Placar.getInt("placar4", 0);
            int placar5  = Placar.getInt("placar5", 0);
            SharedPreferences.Editor editor = Placar.edit();
            if( tentativa < record){
                editor.putInt("record",tentativa );
                record = tentativa;
            }
            placar5 = placar4;
            placar4 = placar3;
            placar3 = placar2;
            placar2 = placar1;
            placar1 = tentativa;
            editor.putInt("placar1",placar1 );
            editor.putInt("placar2",placar2 );
            editor.putInt("placar3",placar3 );
            editor.putInt("placar4",placar4 );
            editor.putInt("placar5",placar5 );
            editor.commit();
            TextView rec = findViewById(R.id.record);
            TextView text1 =  findViewById(R.id.text1);
            TextView text2 =  findViewById(R.id.text2);
            TextView text3 =  findViewById(R.id.text3);
            TextView text4 =  findViewById(R.id.text4);
            TextView text5 =  findViewById(R.id.text5);

            rec.setText(Integer.toString(record));
            text1.setText(Integer.toString(placar1));
            text2.setText(Integer.toString(placar2));
            text3.setText(Integer.toString(placar3));
            text4.setText(Integer.toString(placar4));
            text5.setText(Integer.toString(placar5));
        }
    }
}